#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"management.h"
#include"login.h"
#include"register.h"
#include"mainmenu.h"

// function flush() is to clear the cache
void flush(){
	int ch;
	while((ch = getchar()) != '\n' && ch != EOF)
	{
		;
	}
}
// function removeNewLine() is to remove the '\n'
void removeNewLine(char* string) {

	size_t length = strlen(string);

	if((length > 0) && (string[length-1] == '\n')) {
		string[length-1] ='\0';
	}
	return;
}
int load_books(FILE *file, Book *h){
	Book *p, *last;
	last=h;
	char id[20];
	char tit[20];
	char author[20];
	char years[20];
	char copy[20];
	char num[3]; // to store the number of borrowed book
	while(1){
		if(fgets(id,20,file)){
			removeNewLine(id);
		}
		else{
			break;
		}
		fgets(tit,20,file);
		removeNewLine(tit);
		fgets(author,20,file);
		removeNewLine(author);
		fgets(years,20,file);
		removeNewLine(years);
		fgets(copy,20,file);
		removeNewLine(copy);
		fgets(num,3,file);
		removeNewLine(num);
		p=(Book*)malloc(sizeof(Book));
		p->title=(char*)malloc(sizeof(char));
		p->authors=(char*)malloc(sizeof(char));
		p->id=atoi(id);
		p->year=atoi(years);
		p->copies=atoi(copy);
		p->num=atoi(num);
		strcpy(p->title,tit);
		strcpy(p->authors,author);
		last->next=p;
		last=p;
	}
	last->next=NULL;
	return 0;
}
// to display all books
void showbook(Book *head){
	Book*s=head->next;
	printf("\nID  Tittle               Authors              Years  Copies");
	while(1){
		printf("\n%-3d",s->id);
		printf(" %-20s",s->title);
		printf(" %-20s", s->authors);
		printf(" %-6d", s->year);
		printf(" %-6d\n", s->copies);
		if(s->next==NULL){
			break;
		}
		else{
			s=s->next;
		}
	}
}
Book *add(){
	Book *book;
	book=(Book*)malloc(sizeof(Book));
	int check=0; //to check the invalid input
	char tit[20];
	char aut[20];
	char year[5];
	char copies[5];
	book->title=(char*)malloc(sizeof(char));
	book->authors=(char*)malloc(sizeof(char));
	while(check==0){
		check=1;
		printf("\nEnter the title of the book you wish to add: ");
		gets(tit);
		printf("\nEnter the author of the book you wish to add: ");
		gets(aut);
		printf("\nEnter the year of the book you wish to add: ");
		gets(year);
		printf("\nEnter the number of copies the book you wish to add: ");
		gets(copies);
		if(atoi(copies)==0||atoi(year)==0){
			printf("\nSorry, you attempt to add an invalid book");
			check=0;
		}
	}
	strcpy(book->title, tit);
	strcpy(book->authors, aut);
	book->year=atoi(year);
	book->copies=atoi(copies);
	book->num=0;
	book->next=NULL;
	return book;
}
int add_book(Book book, Book *h){
	Book *head, *p;
	head=h;
	while(head->next!=NULL){
		head=head->next;
		if(strcmp(head->title, book.title)==0){
			printf("Sorry, there is a same book title\n");
			return 1;
		}
	}
	p=(Book*)malloc(sizeof(Book));
	p->title=(char*)malloc(sizeof(char));
	p->authors=(char*)malloc(sizeof(char));
	strcpy(p->title,book.title);
	strcpy(p->authors,book.authors);
	p->year=book.year;
	p->copies=book.copies;
	p->id=head->id+1;
	p->num=0;
	head=h;
	while(head->next!=NULL){
		head=head->next;
	}
	head->next=p;
	p->next=NULL;
	return 0;
}

Book *removeb(Book *h){
	char id[3];
	int ID;
	Book *book;
	book=h->next;
	showbook(h);
	printf("Please choose the ID of the book you want to remove: ");
	gets(id);
	removeNewLine(id);
	while(1){
		ID=book->id;
		if(ID==atoi(id)){
			break;
		}
		else{
			book=book->next;
		}
	}
		return book;
}
int remove_book(Book book,Book *h){
	Book *remove=h;
	Book *premove;
	int count=1;
	while(remove->next!=NULL){
		premove=remove;
		remove=remove->next;
		if(remove->num>0){
			printf("You can not remove this book, because someone borrowed this book");
		}
		if(remove->id==book.id){
			if(remove==h){
				h=remove->next;
			}
			else{
				premove->next=remove->next;
			}
			free(remove);
			Book *head=h->next;
			while(head!=NULL){
				head->id=count;
				head=head->next;
				count++;
			}
			return 0;
		}
	}
	printf("there is no such a book\n");
	return 1;
}

BookList find_book_by_author(const char *author, Book *h){
	Book *head;
	head = h->next;
	BookList *Author;
	Author=(BookList *)malloc(sizeof(BookList));
	Author->list=(Book *)malloc(sizeof(Book));
	Book *p,*last;
	last=Author->list;
	Author->length=0;
	while(head!=NULL){
		if(strcmp(head->authors,author)==0){
			Author->length++;
			p=(Book*)malloc(sizeof(Book));
			p->title=(char*)malloc(sizeof(char));
			p->authors=(char*)malloc(sizeof(char));
			p->id=head->id;
			strcpy(p->title,head->title);
			strcpy(p->authors,head->authors);
			p->year=head->year;
			p->id=head->id;
			p->copies=head->copies;
			last->next=p;
			last=p;
		}
		head=head->next;
	}
	if(Author->length==0){
		Author->list=NULL;
	}
	last->next=NULL;
	return *Author;
}
BookList find_book_by_title(const char *title, Book *h){
	Book *head;
	head = h->next;
	BookList *Title;
	Title=(BookList *)malloc(sizeof(BookList));
	Title->list=(Book *)malloc(sizeof(Book));
	Book *p,*last;
	last=Title->list;
	Title->length=0;
	while(head!=NULL){
		if(strcmp(head->title,title)==0){
			Title->length++;
			p=(Book*)malloc(sizeof(Book));
			p->title=(char*)malloc(sizeof(char));
			p->authors=(char*)malloc(sizeof(char));
			p->id=head->id;
			strcpy(p->title,head->title);
			strcpy(p->authors,head->authors);
			p->year=head->year;
			p->id=head->id;
			p->copies=head->copies;
			last->next=p;
			last=p;
		}
		head=head->next;
	}
	if(Title->length==0){
		Title->list=NULL;
	}
	last->next=NULL;
	return *Title;
}
BookList find_book_by_year(unsigned int year, Book *h){
	Book *head;
	head = h->next;
	BookList *Year;
	Year=(BookList *)malloc(sizeof(BookList));
	Year->list=(Book *)malloc(sizeof(Book));
	Book *p,*last;
	last=Year->list;
	Year->length=0;
	while(head!=NULL){
		if(head->year==year){
			Year->length++;
			p=(Book*)malloc(sizeof(Book));
			p->title=(char*)malloc(sizeof(char));
			p->authors=(char*)malloc(sizeof(char));
			p->id=head->id;
			strcpy(p->title,head->title);
			strcpy(p->authors,head->authors);
			p->year=head->year;
			p->id=head->id;
			p->copies=head->copies;
			last->next=p;
			last=p;
		}
		head=head->next;
	}
	if(Year->length==0){
		Year->list=NULL;
	}
	last->next=NULL;
	return *Year;
}
void findbook(BookList book){
	Book *head;
	if(book.list==NULL){
		printf("no such a book\n");
	}
	else{
		head=book.list->next;
		printf("\nID  Tittle               Authors              Years  Copies");
		while(head!=NULL){
			printf("\n%-3d",head->id);
			printf(" %-20s",head->title);
			printf(" %-20s", head->authors);
			printf(" %-6d", head->year);
			printf(" %-6d\n", head->copies);
			head=head->next;
		}
	}
}
int store_books(FILE *file,Book*h){
	Book *head=h;
	while(1){
		head=head->next;
		fprintf(file, "%d\n",head->id);
		fprintf(file, "%s\n",head->title);
		fprintf(file, "%s\n",head->authors);
		fprintf(file, "%d\n",head->year);
		fprintf(file, "%d\n",head->copies);
		fprintf(file, "%d\n",head->num);
		if(head->next==NULL){
			break;
		}
	}
	return 0;
}
void searchmenu(Book *h){
	float option;
	char *information=(char*)malloc(sizeof(char)*1024);
	while(1){
		printf("\nPlease choose an option:\n");
		printf("1) Find books by tittle\n");
		printf("2) Find books by authors\n");
		printf("3) Find books by years\n");
		printf("4) Return\n");
		printf(" option:");
		scanf("%f",&option);
		flush();
		if(option==1){
			printf("please enter a title: ");
			gets(information);
			removeNewLine(information);
			findbook(find_book_by_title(information,h));
		}
		else if(option==2){
			printf("please enter a author: ");
			gets(information);
			removeNewLine(information);
			findbook(find_book_by_author(information,h));
		}
		else if(option==3){
			printf("please enter a year: ");
			gets(information);
			if(atoi(information)==0){
				printf("input valid\n");
			}
			else{
				findbook(find_book_by_year(atoi(information),h));
			}
		}
		else if(option==4){
			break;
		}
		else{
			printf("\nSorry, the option you entered was invalid, please try again.");
		}
	}

}
//after librarian logging
void librarianmenu(Book *head){
	float loption;
	while(1){
		printf("\nPlease choose an option:\n");
		printf("1) Add a book\n");
		printf("2) Remove a book\n");
		printf("3) Search for books\n");
		printf("4) Display all books\n");
		printf("5) Log out\n");
		printf(" option:");
		scanf("%f",&loption);
		flush();
		if(loption==1){
			add_book(*add(),head);
		}
		else if(loption==2){
			remove_book(*removeb(head),head);;
		}
		else if(loption==3){
			searchmenu(head);
		}
		else if(loption==4){
			showbook(head);
		}
		else if(loption==5){
			break;
		}
		else{
			printf("\nSorry, the option you entered was invalid, please try again.");
		}
	}
}
int main(int argc, char **argv){
	char book[20];
	char test;
	float check;
	char *name;
	BookList *booklist;
	booklist=(BookList *)malloc(sizeof(BookList));
	booklist->list=(Book *)malloc(sizeof(Book));
	Book *hb;
	hb=(Book*)malloc(sizeof(Book));
	FILE *fp=fopen("borrow.txt","a+");
	test=fgetc(fp);
	if(test==EOF){
		fprintf(fp,"librarian\n");
		fprintf(fp,"0\n");
	}
	fclose(fp);
	FILE *fp4=fopen("username.txt","a+");
	test=fgetc(fp4);
	if(test==EOF){
		fprintf(fp4,"librarian\n");
	}
	fclose(fp4);
	FILE *fp5=fopen("userpw.txt","a+");
	test=fgetc(fp5);
	if(test==EOF){
		fprintf(fp5,"librarian\n");
	}
	fclose(fp5);
	printf("To load a book file or create a new file(Loading a file in the wrong format will cause the program to run incorrectly)?\n");
	printf("Input 1 to load file, 2 to create a new file:  ");
	scanf("%f",&check);
	flush();
	if(check==2){
		printf("Please enter the file name: ");
		gets(book);
		FILE *fp1;
		fp1=fopen(book,"w");
		fprintf(fp1,"1\n");
		fprintf(fp1,"test\n");
		fprintf(fp1,"test\n");
		fprintf(fp1,"2022\n");
		fprintf(fp1,"1\n");
		fprintf(fp1,"0\n");
		fclose(fp1);
		fp1=fopen(book,"r");
		load_books(fp1,hb);
		fclose(fp1);
	}
	if(check==1){
		printf("Please enter the file name: ");
		gets(book);
		FILE *fp1;
		fp1=fopen(book,"r");
		if(fp1==NULL){
			printf("Book file does not exist");
			exit(0);
		}
		load_books(fp1,hb);
		fclose(fp1);
	}
	float option=-1;//to store the input option
	while(1){
		mainmenu();
		scanf("%f",&option);
		flush();
		if(option==1){
			reg();
		}
		else if(option==2){
			name=login();
			if(strcmp(name,"librarian")==0){
				librarianmenu(hb);
			}
			else{
				usermenu(hb,name);
			}
		}
		else if(option==3){
			searchmenu(hb);
		}
		else if(option==4){
			showbook(hb);
		}
		else if(option==5){
			FILE *fp3;
			fp3=fopen(book,"w");
			store_books(fp3,hb);
			fclose(fp3);
			return 0;
		}
		else{
			printf("\nSorry, the option you entered was invalid, please try again.");
		}
	}
}


