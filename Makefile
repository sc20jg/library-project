
# code details

EXE = ./library
SRC= main.c login.c mainmenu.c register.c user.c

# generic build details

CC=      gcc
CFLAGS= -std=c99 -Wall
CLINK= 

# compile to object code

OBJ= $(SRC:.c=.o)

.c.o:
	$(CC) $(CFLAGS) -c -o $@ $<

# build executable: type 'make'

$(EXE): $(OBJ)
	$(CC) $(OBJ) $(CLINK) -o $(EXE) 
all: 
	$(CC) $(CFLAGS) $(SRC) -o $(EXE) 


# clean up and remove object code and executable: type 'make clean'

clean:
	rm -f $(OBJ) $(EXE)

# dependencies

user.o: user.c management.h
main.o:      main.c management.h login.h register.h mainmenu.h
login.o:   login.c login.h management.h
mainmenu.o: mainmenu.c mainmenu.h
register.o:      register.c register.h management.h

