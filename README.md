Project Name：library project

URL: git@gitlab.com:sc20jg/library-project.git

    https://gitlab.com/sc20jg/library-project.git

History:
[history photo](https://gitlab.com/sc20jg/library-project/-/blob/main/_LS1_A_SMWNXRM_DCS_UW_K.png)

Description
This is the first coursework of the programming project, which aims to build a proposed library management system. I will be using gitlab to document my development process in full, so earlier versions are not runnable and only allow for certain modular features.

version
v1.3.0

Several bugs have been fixed, all functions have been implemented and the makefile is available, the program now runs properly.

Updated on 7 April

v1.2.0

Several bugs have been fixed, all functions have been implemented and the makefile is available, the program now runs properly.

Updated on 4 April

v1.1.0 

The development of all the functions has been almost completed, but the modules for most of the functions have not yet been implemented. For testing purposes, most of the functions have been placed directly in main.c. Also the makefile is still not complete, so the program is still not guaranteed to run in any environment.

Updated on 29 March

v1.0.3 The initial main function is built and the program now runs the main menu, registration and login functions normally.

This version is only used to document my development process, so it is still not runnable.

Updated on 25 March
 
v1.0.2 Updated the code for the login function

This version is only used to document my development process, so it is still not runnable.

Updated on 22 March

v1.0.1  Updated registration function

This version is only used to document my development process, so it is still not runnable.

Updated on 21 March
