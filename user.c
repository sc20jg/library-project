#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"management.h"

//after user logging
void usermenu(Book *head,const char *name){
	float loption;
	while(1){
		printf("\nPlease choose an option:\n");
		printf("1) Borrow a book\n");
		printf("2) Return a book\n");
		printf("3) Search for books\n");
		printf("4) Display all books\n");
		printf("5) Log out\n");
		printf(" option:");
		scanf("%f",&loption);
		flush();
		if(loption==1){
			borrowbook(head,name);
		}
		else if(loption==2){
			returnbook(head,name);
		}
		else if(loption==3){
			searchmenu(head);
		}
		else if(loption==4){
			showbook(head);
		}
		else if(loption==5){
			break;
		}
		else{
			printf("\nSorry, the option you entered was invalid, please try again.");
		}
	}
}
int borrowbook(Book *head,const char *name){
	char username[15];
	char id[3];
	char Id[3];
	unsigned int ID;
	int check=1;//to check if the username is in the file
	int Check=1;//to check if the user has borrowed a book
	int CHeck=1;//to check the book user borrowed
	Book *he=head->next;
	FILE *fp1;
	FILE *fp2;
	fp1=fopen("borrow.txt","r+");
	fp2=fopen("test.txt","w");
	while(1){
		if(fgets(username,15,fp1)){
			removeNewLine(username);
		}
		else{
			break;
		}
		if(fgets(id,3,fp1)){
			removeNewLine(id);
		}
		if(strcmp(username,name)==0){
			check=0;
			if(atoi(id)!=0){
				Check=0;
			}
			break;
		}
	}
	if (check==0&&Check==0){
		printf("you can not borrow more than one book\n");
		fclose(fp1);
		return 0;
	}
	else{
		printf("Please choose an ID of the book you want to borrow: ");
		fgets(id,3,stdin);
		removeNewLine(id);
		strcpy(Id,id);
		ID=atoi(id);
		while(1){
			if(he->id==ID){
				CHeck=0;
				break;
			}
			if(he->next==NULL){
				break;
			}
			he=he->next;
		}
		if(CHeck!=0){
			printf("Sorry, there is no such a book\n");
			fclose(fp1);
			return 0;
		}
		he->copies=he->copies-1;
		he->num=he->num+1;
		if(check!=0){
			fputs (name, fp1);
			fputs("\n",fp1);
			fputs (id, fp1);
			fclose(fp1);
			fclose(fp2);
			remove("test.txt");
		}
		rewind(fp1);
		while(1){
			if(fgets(username,15,fp1)){
				fputs(username,fp2);
				removeNewLine(username);
			}
			else{
				break;
			}
			if(strcmp(username,name)==0){
				fputs (Id, fp2);
				fputs("\n",fp2);
				fgets(id,3,fp1);
			}
			else if(fgets(id,3,fp1)){
				fputs(id,fp2);
			}
		}
		fclose(fp1);
		fclose(fp2);
		remove("borrow.txt");
		rename("test.txt","borrow.txt");
		return 0;
	}
}
int returnbook(Book *head,const char *name){
	char username[15];
	char id[3];
	char Id[3];
	unsigned int ID;
	int check=1;//check if the username is in the file
	int Check=1;//check if the user has borrowed a book
	int CHeck=1;//check the book user returned
	Book *he=head->next;
	FILE *fp1;
	FILE *fp2;
	fp1=fopen("borrow.txt","r+");
	fp2=fopen("test.txt","w");
	if(fp1==NULL){
		printf("borrow file does not exist");
		return 1;
	}
	while(1){
		if(fgets(username,15,fp1)){
			removeNewLine(username);
		}
		else{
			break;
		}
		if(fgets(id,3,fp1)){
			removeNewLine(id);
		}
		if(strcmp(username,name)==0){
			check=0;
			if(atoi(id)!=0){
				Check=0;
			}
			break;
		}
	}
	if (check==1||Check==1){
		printf("you did not borrow a book\n");
		fclose(fp1);
		return 0;
	}
	else{
		printf("Please choose an ID of the book you want to return: ");
		fgets(Id,3,stdin);
		removeNewLine(Id);
		ID=atoi(id);
		if(atoi(id)!=atoi(Id)){
			printf("Sorry, you did not borrow this book\n");
			fclose(fp1);
			return 0;
		}
		while(1){
			if(he->id==ID){
				CHeck=0;
				break;
			}
			if(he->next==NULL){
				break;
			}
			else{
				he=he->next;
			}
		}
		if(CHeck!=0){
			printf("Sorry, there is no such a book\n");
			fclose(fp1);
			return 0;
		}
		he->copies=he->copies+1;
		he->num=he->num-1;
		rewind(fp1);
		while(1){
			if(fgets(username,15,fp1)){
				fputs(username,fp2);
				removeNewLine(username);
			}
			else{
				break;
			}
			if(strcmp(username,name)==0){
				fputs ("0\n", fp2) ;
				fgets(id,3,fp1);
			}
			else if(fgets(id,3,fp1)){
				fputs(id,fp2);
			}
		}
		fclose(fp1);
		fclose(fp2);
		remove("borrow.txt");
		rename("test.txt","borrow.txt");
		return 0;
	}
}
